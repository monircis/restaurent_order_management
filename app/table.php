<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class table extends Model
{
    protected $fillable = [
        'table_id'
    ];
}
