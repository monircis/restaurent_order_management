<?php

namespace App\Http\Controllers;
Use App\payment_type;
Use App\customer;
Use App\order;
Use App\payment;
Use App\orderitem;
Use DB;
use Illuminate\Http\Request;

class paymentsController extends Controller
{
    public  function  index(){
        return view('payments.payment-type');
    }
    public  function  create(Request $request){
        $payments_type =  new payment_type();
        $payments_type->paymentTypeName = $request->paymentTypeName;
        $payments_type->save();
    }
    public  function  show_payment_type(){
        $payments_type = payment_type::all();
        return view('payments.payment-type-list',['payments_type'=> $payments_type]);

    }
    public  function  bill_pay($orderId){
        $customer_query = DB::table('orders')
            ->join('customers', function ($join) use ($orderId) {
                $join->on('orders.customerId', '=', 'customers.id')
                ->where('orders.orderId', '=',  $orderId);
            })->first();

        $order_items_query = DB::table('orderitems')
            ->join('foodmenus', function ($join) use ($orderId) {
                $join->on('orderitems.foodId', '=', 'foodmenus.id')
                    ->where('orderitems.orderId', '=',  $orderId);
            })->get();

            $calculate_total =   DB::table('payments')->where('order_id', '=', $orderId)->first();


//dd($calculate_total);
//exit();
        return view('payments.pay-payment',['customer'=>$customer_query,'order_items'=>$order_items_query,
            'total'=>$calculate_total]);

    }




}
