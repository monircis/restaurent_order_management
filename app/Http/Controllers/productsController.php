<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use App\foodmenu;
use DB;
class productsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function category()
    {
       return view('products.category');
    }
      public function foodmenu()
    {
        $categories = category::all();
        return view('products.foodmenu',['categories'=> $categories]);
    }
  
    /**
     * Show the form for creating a new Category.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_category(Request $request)
    { 
         $categories =  new category(); 
        $categoryName = $request->categoryName;
        $categories->categoryName = $categoryName;
        $categories->save();
        $request->session()->flash('success', 'New Category  added successfully!');
        return redirect('category');
        
    }
     public function create_foodmenu(Request $request)
    {

        $foodmenus =  new foodmenu();
        $foodmenus->categoryId = $request->categoryId;
        $foodmenus->foodName = $request->foodName;
        $foodmenus->quantity = $request->quantity;
        $foodmenus->price = $request->price;
        $foodmenus->vat = $request->vat;
       $foodmenus->save();
        $request->session()->flash('success', 'New Food  added successfully!');
        return redirect('foodmenu');
        
    }
    
    
     /**
     * Show the form for show categories .
     *
     * @return \Illuminate\Http\Response
     */
    public function categories()
    { 
       $categories = category::all();
       return view('products.category-list',['categories'=> $categories]);
         
         
        
    }
    
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
