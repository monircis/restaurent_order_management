<?php

namespace App\Http\Controllers;

use App\order;
use App\customer;
use App\orderitem;
use App\payment;
use Auth;
use DB;
use Illuminate\Http\Request;

class ordersController extends Controller
{
    /**
     * Display  order  create  page.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('orders.index');
    }

    /**
     * Store a newly created order  in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request)
    {

        $customers = new customer();
        $customers->name = $request->name;
        $customers->phone = $request->phone;
        $customers->address = $request->address;
        $customers->save();
        $customerId = $customers->id;


        $userId = Auth::id();

        $orders = new order();
        $orders->numberOfPerson = $request->numberOfPerson;
        $orders->customerId = $customerId;
        $orders->tableId = '1';
        $orders->userId = $userId;
        $orders->save();
        $orderId = $orders->id;

        //payment  table data  insert
        $payments =  new payment();
        $payments->order_Id = $orderId;
        $payments->orderTotal =  $request->orderTotal;
        $payments->paymentTypeId =  '3';
        $payments->order_Id = $orderId;
        $payments->save();

        $items = $request->foodId;
        foreach ($items as $item) {
            $orderitems = new orderitem();
            $orderitems->orderId = $orderId;
            $orderitems->foodId = $item;
            $orderitems->quantity = '3';
            $orderitems->total = '30';
            $orderitems->discount = '10';
            $orderitems->save();
        }
    }

    public function show_orders()
    {

        $order_details = DB::table('orders')
            ->join('customers', 'orders.customerId', '=', 'customers.id')
            ->join('users', 'orders.userId', '=', 'users.id')
            //->join('orderitems', 'orders.orderId', '=', 'orderitems.orderId')
            ->join('payments', 'orders.orderId', '=', 'payments.order_id')
            ->select('orders.orderId', 'orders.numberOfPerson', 'orders.tableId','payments.orderTotal', 'orders.created_at', 'customers.phone',
                'customers.name as customerName', 'users.name')
            ->get();
        return view('orders.order-list', ['order_details' => $order_details]);


//dd($order_details);
//exit();
//        $calculate_total = DB::table('orders')
//            ->select('orders.orderId',  DB::raw('SUM(total) as total'))
//            ->join('orderitems', 'orderitems.orderId', '=', 'orders.orderId')
//            ->groupBy('orders.orderId')
//            ->get();





    }

}
