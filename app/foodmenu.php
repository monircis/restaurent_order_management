<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class foodmenu extends Model
{
      protected $fillable = [
        'categoryId','foodName','quantity','price','vat'
    ];
}
