<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_Id')->unsigned();
            $table->foreign('order_Id')->references('orderId')->on('orders')->onDelete('cascade');
            $table->integer('paymentTypeId')->unsigned();
            $table->foreign('paymentTypeId')->references('id')->on('payment_types')->onDelete('cascade');
            $table->integer('orderTotal');
            $table->integer('discountOnTotal');
            $table->integer('paidAmount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
