<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

 
 
Route::get('/category', 'productsController@category');
Route::post('/create_category', 'productsController@create_category');
Route::get('category/lists', 'productsController@categories');

Route::get('/foodmenu', 'productsController@foodmenu');
Route::post('/create_foodmenu', 'productsController@create_foodmenu');

Route::get('/table', 'tablesController@index');
Route::post('/create_table', 'tablesController@create_table');
Route::get('/table/lists', 'tablesController@show_tables');


Route::get('/order', 'ordersController@index');
Route::post('create_order', 'ordersController@create');
Route::get('/order/lists', 'ordersController@show_orders');


Route::get('/paymenttype', 'paymentsController@index');
Route::post('paymenttype/create', 'paymentsController@create');
Route::get('paymenttype/lists', 'paymentsController@show_payment_type');
Route::get('payment/billpay/{orderId}', 'paymentsController@bill_pay');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', function(){
    echo "Hello Admin";
})->middleware('auth','admin');

Route::get('/agent', function(){
    echo "Hello Agent";
})->middleware('auth','agent');

Route::get('/customer', function(){
    echo "Hello Customer";
})->middleware('auth','customer');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
