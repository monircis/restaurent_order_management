@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Table List</div>

                <div class="panel-body">
                               <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Table Id</th>
      <th scope="col"> </th>
    </tr>
  </thead>
  <tbody>
  
    @foreach($tables as $table)
     <tr>
      <th scope="row">1</th>
     <td>{{$table->table_id}}</td>
       
      <td><a href="{{url('/'.$table->table_id)}}">Edit</a>
      <a href="">Delete</a>
      </td>
    </tr>
                                   
   @endforeach   
   
     
  </tbody>
</table>
                   
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
