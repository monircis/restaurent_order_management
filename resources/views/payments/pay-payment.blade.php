@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        {!! Form::open(['url' => 'paybill','class'=>'form-horizontal','enctype'=>'multipart/form-data','files'=>true ]) !!}
        {{ csrf_field() }}


        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Customer Information</div>

                <div class="panel-body">

                                   
                                           
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label"> Customer Name <i style="color: red;">*</i></label>
                                                <div class="col-lg-7">
                                                 <div class="form-group">
                                                    <input class="form-control" name="name"  value="{{$customer->name}}" type="text" required="">
                                                     </div>
                                                </div>
                                            </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Type phone <i style="color: red;">*</i></label>
                        <div class="col-lg-7">
                            <div class="form-group">
                                <input class="form-control" name="phone"  type="text"  value="{{$customer->phone}}" required="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Type address <i style="color: red;">*</i></label>
                        <div class="col-lg-7">
                            <div class="form-group">
                                <input class="form-control" name="address"  type="text" value="{{$customer->address}}" required="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">numberOfPerson <i style="color: red;">*</i></label>
                        <div class="col-lg-7">
                            <div class="form-group">
                                <input class="form-control" name="numberOfPerson"  type="text"  value="{{$customer->numberOfPerson}}" required="">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6 ">
            <div class="panel panel-default">
                <div class="panel-heading">Order Information</div>

                <div class="panel-body">

                    @foreach($order_items as $order_item)
                         <label>foodName: {{$order_item->foodName}} <br>
                             price:  {{$order_item->price}} <br>
                             vat: {{$order_item->vat}}<br>
                             quantity: {{$order_item->quantity}}<br>
                             total: {{$order_item->total}}<br>
                             quantity: {{$order_item->discount}}
                         </label>
<hr>
                    @endforeach


==================================
                        Total Amount =  {{$total->orderTotal}}
                        <input type="text" name="discountOnTotal"  placeholder="discountOnTotal">
                        <input type="text" name="netpay" placeholder="netpay">
                        <input type="text" name="paidAmount" placeholder="paidAmount">
                    <div class="form-group">
                        <label class="col-lg-4 control-label"></label>
                        <div class="col-lg-7">
                            <button type="submit" class="btn btn-custom btn-bordred waves-effect waves-light w-md m-b-5 pull-left"><i class="fa  fa-save"></i> Save</button>

                        </div>
                    </div>





                </div>
            </div>
        </div>

        {!! Form::close() !!}
    </div>
</div>
@endsection
