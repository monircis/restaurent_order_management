@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Category</div>

                    <div class="panel-body">
                        {!! Form::open(['url' => 'create_foodmenu','class'=>'form-horizontal','enctype'=>'multipart/form-data','files'=>true ]) !!}
                        {{ csrf_field() }}


                        <div class="form-group">
                            <label class="col-lg-4 control-label">Type Food Caregory <i
                                        style="color: red;">*</i></label>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <select class="form-control" name="categoryId">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->categoryName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Type Food Name <i style="color: red;">*</i></label>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <input class="form-control" name="foodName" type="text" required="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Type Food quantity <i
                                        style="color: red;">*</i></label>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <input class="form-control" name="quantity" type="text" required="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Type Food price <i style="color: red;">*</i></label>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <input class="form-control" name="price" type="text" required="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Type Food vat <i style="color: red;">*</i></label>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <input class="form-control" name="vat" type="text" required="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label"></label>
                            <div class="col-lg-7">
                                <button type="submit"
                                        class="btn btn-custom btn-bordred waves-effect waves-light w-md m-b-5 pull-left">
                                    <i class="fa  fa-save"></i> Save
                                </button>

                            </div>
                        </div>


                        {!! Form::close() !!}


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
