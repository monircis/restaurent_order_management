@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Category List</div>

                <div class="panel-body">
                               <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Category Name</th>
       
      <th scope="col"> </th>
    </tr>
  </thead>
  <tbody>
  
    @foreach($categories as $category)
     <tr>
      <th scope="row">1</th>
     <td>{{$category->categoryName}}</td>
       
      <td><a href="{{url('/'.$category->id)}}">Edit</a>
      <a href="">Delete</a>
      </td>
    </tr>
                                   
   @endforeach   
   
     
  </tbody>
</table>
                   
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
