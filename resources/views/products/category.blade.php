@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Category</div>

                <div class="panel-body">
                              {!! Form::open(['url' => 'create_category','class'=>'form-horizontal','enctype'=>'multipart/form-data','files'=>true ]) !!}
                                  {{ csrf_field() }}
                                   
                                           
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label">Type Category Name <i style="color: red;">*</i></label>
                                                <div class="col-lg-7">
                                                 <div class="form-group">
                                                    <input class="form-control" name="categoryName"  type="text" required="">
                                                     </div>
                                                </div>
                                            </div>
                                             
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label"></label>
                                                <div class="col-lg-7">
                                                     <button type="submit" class="btn btn-custom btn-bordred waves-effect waves-light w-md m-b-5 pull-left"><i class="fa  fa-save"></i> Save</button>
                                    
                                                </div>
                                            </div>
                                       
                                     
                                {!! Form::close() !!}
                   
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
