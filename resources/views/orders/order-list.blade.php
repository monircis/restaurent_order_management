@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Order List</div>

                <div class="panel-body">
                               <table class="table table-striped">
  <thead>
    <tr>

      <th scope="col">Order Id</th>
      <th scope="col">numberOfPerson</th>
        <th scope="col">tableId</th>
        <th scope="col">customer Name</th>
        <th scope="col">customer Phone</th>
        <th scope="col">userId</th>
        <th scope="col">total</th>
        <th scope="col">Date</th>
    </tr>
  </thead>
  <tbody>
  
@foreach($order_details as $singleOrder)
     <tr>
      <th scope="row">{{$singleOrder->orderId}}</th>
     <td>{{$singleOrder->numberOfPerson}}</td>
       
      <td>{{$singleOrder->tableId}}</td>
         <td>{{$singleOrder->customerName}}</td>
         <td>{{$singleOrder->phone}}</td>
         <td>{{$singleOrder->name}}</td>
         <td>{{$singleOrder->orderTotal}}</td>
         <td>{{$singleOrder->created_at}} <a href="{{ url('payment/billpay',$singleOrder->orderId )}}">Pay Bill</a></td>

    </tr>
                                   
@endforeach
  </tbody>
</table>
                   
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
