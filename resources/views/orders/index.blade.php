@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        {!! Form::open(['url' => 'create_order','class'=>'form-horizontal','enctype'=>'multipart/form-data','files'=>true ]) !!}
        {{ csrf_field() }}


        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Customer Information</div>

                <div class="panel-body">

                                   
                                           
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label">Type Customer Name <i style="color: red;">*</i></label>
                                                <div class="col-lg-7">
                                                 <div class="form-group">
                                                    <input class="form-control" name="name"  type="text" required="">
                                                     </div>
                                                </div>
                                            </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Type phone <i style="color: red;">*</i></label>
                        <div class="col-lg-7">
                            <div class="form-group">
                                <input class="form-control" name="phone"  type="text" required="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Type address <i style="color: red;">*</i></label>
                        <div class="col-lg-7">
                            <div class="form-group">
                                <input class="form-control" name="address"  type="text" required="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">numberOfPerson <i style="color: red;">*</i></label>
                        <div class="col-lg-7">
                            <div class="form-group">
                                <input class="form-control" name="numberOfPerson"  type="text" required="">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6 ">
            <div class="panel panel-default">
                <div class="panel-heading">Order Information</div>

                <div class="panel-body">




                    <div class="form-group">
                        <label class="col-lg-4 control-label">option <i style="color: red;">*</i></label>
                        <div class="col-lg-7">
                             <label><input type="checkbox" value="1" name="foodId[]" /> A</label>
                            <label><input type="checkbox" value="2" name="foodId[]" /> B</label>
                            <label><input type="checkbox" value="3" name="foodId[]" /> C</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Total  <i style="color: red;">*</i></label>
                        <div class="col-lg-7">
                            <div class="form-group">
                                <input class="form-control" name="orderTotal"  type="text" required="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"></label>
                        <div class="col-lg-7">
                            <button type="submit" class="btn btn-custom btn-bordred waves-effect waves-light w-md m-b-5 pull-left"><i class="fa  fa-save"></i> Save</button>

                        </div>
                    </div>





                </div>
            </div>
        </div>

        {!! Form::close() !!}
    </div>
</div>
@endsection
